extern crate getopts;

use getopts::Options;
use std::env;
use std::fs::File;
use std::io::Read;

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options] FILE", program);
    print!("{}", opts.usage(&brief));
}

fn do_cat(input_files: Vec<String>, o_number: bool) {
    for file_name in input_files {
        println!(":{}", file_name);

        //let mut f = try!(File::open(file_name));
        let mut f = match File::open(file_name.clone()) {
            Ok(m) => { m },
            Err(f) => { panic!(f.to_string()) },
        };
        let mut s = String::new();
        f.read_to_string(&mut s).unwrap();

        if o_number {
            println!("-n    {}", o_number);
            let split = s.split("\n");
            let vec = split.collect::<Vec<&str>>();
            let mut i = 1;
            for part in vec {
                if part.len() <1 {
                    continue;
                }
                print!("    {}", i);
                print!("  {}\n", part);
                i = i+1
            }
        } else {
            //try!(f.read_to_string(&mut s));
            print!("{}", s);
        }
    }
    //println!("{}", inp);
    //match out {
    //    Some(x) => println!("{}", x),
    //    None => println!("No output"),
    //}
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optflag("n", "number", "number all output lines");
    opts.optflag("h", "help", "print help");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m },
        Err(f) => { panic!(f.to_string()) }
    };
    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }
    let o_number = matches.opt_present("n");

    let input = if !matches.free.is_empty() {
        matches.free.clone()
    } else {
        print_usage(&program, opts);
        return;
    };
    do_cat(input, o_number);
}
